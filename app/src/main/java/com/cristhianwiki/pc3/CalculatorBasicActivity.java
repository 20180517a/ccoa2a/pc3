package com.cristhianwiki.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.material.snackbar.Snackbar;

public class CalculatorBasicActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etNum1;
    EditText etNum2;

    Button btnAdd;
    Button btnSub;
    Button btnMult;
    Button btnDiv;
    Button btnClean;
    Button btnResult;
    Button btnBack;

    TextView tvResult;

    String oper = "";
    String resultText = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_basic);

        etNum1 = findViewById(R.id.etNum1);
        etNum2 = findViewById(R.id.etNum2);

        btnAdd = findViewById(R.id.btnAdd);
        btnSub = findViewById(R.id.btnSub);
        btnMult = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDiv);
        btnClean = findViewById(R.id.btnClean);
        btnResult = findViewById(R.id.btnResult);
        btnBack = findViewById(R.id.btnBack);

        tvResult = findViewById(R.id.tvResult);

        btnAdd.setOnClickListener(this);
        btnSub.setOnClickListener(this);
        btnMult.setOnClickListener(this);
        btnDiv.setOnClickListener(this);


        btnClean.setOnClickListener(v -> {
            tvResult.setText("");
            etNum1.setText("");
            etNum2.setText("");
        });

        btnResult.setOnClickListener(v -> {
            if(resultText.equals("")){
                Snackbar.make(v, R.string.validar_input,Snackbar.LENGTH_LONG).show();
            }else{
                tvResult.setText(resultText);
            }
        });

        btnBack.setOnClickListener(v -> {
            Intent intent = new Intent(CalculatorBasicActivity.this, MainActivity.class);
            startActivity(intent);
        });


        /*
        TextWatcher textWatcher =
                new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        String userNameText = WTTwitterLoginActivity.this.userName.getText().toString();
                        String passwordText = WTTwitterLoginActivity.this.password.getText().toString();
                        if (!userNameText.contentEquals("") && !passwordText.contentEquals("")) {
                            loginButton.setEnabled(true);
                        } else {
                            loginButton.setEnabled(false);
                        }
                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                };
        etNum1.addTextChangedListener(textWatcher);
        */
    }

    @Override
    public void onClick(View v) {
        float num1;
        float num2;
        float result = 0;

        if (etNum1.getText().toString().equals("")
                || etNum2.getText().toString().equals("")) {
            Snackbar.make(v, R.string.validar_input,Snackbar.LENGTH_LONG).show();
            return;
        }

        num1 = Float.parseFloat(etNum1.getText().toString());
        num2 = Float.parseFloat(etNum2.getText().toString());

        // definimos el botón en el que se ha hecho clic y realizamos
        // la operación correspondiente operación de escritura en oper, lo usaremos más tarde para la salida
        switch (v.getId()) {
            case R.id.btnAdd:
                oper = "+";
                result = num1 + num2;
                break;
            case R.id.btnSub:
                oper = "-";
                result = num1 - num2;
                break;
            case R.id.btnMult:
                oper = "*";
                result = num1 * num2;
                break;
            case R.id.btnDiv:
                oper = "/";
                if(etNum2.getText().toString().equals("0")){
                    Snackbar.make(v, R.string.validar_input,Snackbar.LENGTH_LONG).show();
                    return;
                }
                result = num1 / num2;
                break;
            default:
                break;
        }
        resultText = num1 + " " + oper + " " + num2 + " = " + result;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }
}