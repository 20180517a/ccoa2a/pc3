package com.cristhianwiki.pc3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    RadioGroup radioGroup;
    RadioButton radioButton1,radioButton2,radioButton3;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.radio_group);
        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> {
            if(radioButton1.isChecked()){
                Intent intent = new Intent(MainActivity.this,CalculatorBasicActivity.class);
                startActivity(intent);
            }
            if(radioButton2.isChecked()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder
                        .setTitle(R.string.dialog_title)
                        .setMessage(R.string.dialog_msg)
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            dialog.cancel();
                        })
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                            //Salir del aplicativo
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }).show();
            }
            if(radioButton3.isChecked()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder
                        .setTitle(R.string.dialog_title)
                        .setMessage(R.string.dialog_msg)
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            dialog.cancel();
                        })
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                            //Salir del aplicativo
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }).show();
            }
        });
    }
}